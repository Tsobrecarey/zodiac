<!DOCTYPE html>
<html>
<head>
	<title>Movies</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/slate/bootstrap.css">
</head>
<body>
	<h1 class="text-center">Because I can!!!</h1>
	
	<?php
		$movies=[["title"=>"Psycho", "year"=>"1960", "director"=>"Alfred Hitchcock", "genre"=>"Horror"], ["title"=>"Azumi", "year"=>"2003", "director"=>"Ryûhei Kitamura", "genre"=>"Drama"], ["title"=>"Eurotrip", "year"=>"2004", "director"=>"Jeff Schaffer", "genre"=>"Comedy"]];
	?>
	<div class="container">
		<div class="row">
			<?php
				foreach ($movies as $movie) {
			?>
			<div class="card mx-2 my-3 w-25">
				<h3 class="text-center">Movie Title: <?php echo $movie["title"];?></h3>
				<p class="text-center">Director: <?php echo $movie["director"];?></p>
				<p class="text-center">Year: <?php echo $movie["year"];?></p>
				<p class="text-center">Genre: <?php echo $movie["genre"];?></p>
			</div>
			<?php
				}
			?>
		</div>
	</div>
</body>
</html>