<?php
	$firstName=$_POST['firstName'];
	$lastName=$_POST['lastName'];
	
	if(strlen($firstName)==0 || strlen($lastName)==0){
		$_SESSION['errorMsg']="Leave no stones unturned";
		header("Location: ". $_SERVER['HTTP_REFERER']);
	}else{
		session_start();
		$_SESSION['firstName'] = $firstName;
		$_SESSION['lastName'] = $lastName;
		header("Location: ../views/landingpage.php");
	}
?>