<?php
	$name=$_POST['name'];
	$bmonth=$_POST['bmonth'];
	$bday=$_POST['bday'];

	if(strlen($name)==0 || strlen($bmonth)==0 || strlen($bday)==0){
		session_start();
		$_SESSION['errorMsg']="Leave no stone unturned";
		header("Location: ". $_SERVER['HTTP_REFERER']);
	} else if(($bmonth)<=0 || ($bmonth)>12){
		session_start();
		$_SESSION['errorMsg']="Invalid Month";
		header("Location: ". $_SERVER['HTTP_REFERER']);
	} else if(($bday)<=0 || ($bday)>31){
		session_start();
		$_SESSION['errorMsg']="Invalid Day";
		header("Location: ". $_SERVER['HTTP_REFERER']);
	}else {
	session_start();
	$_SESSION['name']=$name;
	$_SESSION['bmonth']=$bmonth;
	$_SESSION['bday']=$bday;
	header("Location: ../views/horoscopev1.php");
	}
?>
