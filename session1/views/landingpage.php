<!DOCTYPE html>
<html>
<head>
	<title>Landing Page</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/slate/bootstrap.css">
</head>
<body>
	<div class="d-flex justify-content-center align-items-center vh-100">
		<?php
			session_start();
		?>
		<h1>Hello <?php echo $_SESSION['firstName']. " " . $_SESSION['lastName']?></h1>
	</div>
</body>
</html>