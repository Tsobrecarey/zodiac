<!DOCTYPE html>
<html>
<head>
	<title>Registration Form</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/slate/bootstrap.css">
</head>
</head>
<body class="bg-primary">
	<h1 class="text-center text-white m-5">Registration Form</h1>
	<div class="col-lg-4 offset-lg-4">
		<form action="controllers/registration-form.php" class="bg-secondary p-3" method="POST">
			<div class="form-group">
				<label class="firstName text-dark">First Name</label>
				<input type="text" name="firstName" class="form-control">
			</div>
			<div class="form-group">
				<label class="lastName text-dark">Last Name</label>
				<input type="text" name="lastName" class="form-control">
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-primary">Register</button>
			</div>
			<?php
				session_start();
				if(isset($_SESSION['errorMsg'])){
			?>
			<p class="text-center text-dark"><?php echo $_SESSION['errorMsg']?></p>
			<?php
				}
			?>
		</form>		
	</div> 
</body>
</html>