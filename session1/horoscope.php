<!DOCTYPE html>
<html>
<head>
	<title>What is Horoscope</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/slate/bootstrap.css">
</head>
<body>
	<h1 class="text-center text-white m-5">Learn your sign!!!</h1>
	<div class="col-lg-4 offset-lg-4">
		<form action="controllers/horoscopep.php" class="bg-secondary p-3" method="POST">
			<div class="form-group">
				<label class="name text-dark">Name</label>
				<input type="text" name="name" class="form-control">					
			</div>
			<div class="form-group">
				<label class="bmonth text-dark">Birth Month</label>
				<input type="number" name="bmonth" class="form-control">					
			</div>
			<div class="form-group">
				<label class="bday text-dark">Birth Day</label>
				<input type="number" name="bday" class="form-control">					
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-primary">!!!Check Your Zodiac Sign!!!</button>
			</div>
			<?php
				session_start();
				if(isset($_SESSION['errorMsg'])){
			?>
			<p class="text-center text-dark"><?php echo $_SESSION['errorMsg']?></p>
			<?php
				}
			?>
		</form>			
	</div>

</body>
</html>